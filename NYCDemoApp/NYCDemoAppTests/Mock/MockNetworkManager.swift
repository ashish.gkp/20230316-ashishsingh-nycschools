//
//  MockNetworkManager.swift
//  NYCDemoAppTests
//
//  Created by Ashish Singh on 3/15/23.
//

import Foundation
@testable import NYCDemoApp

class MockNetworkManager: NetworkHandler {
    var mockResult: Result<[SchoolDataModel], AppErrors>?
    var isFetchDataCalled = false
    func fetchSchools(completion: @escaping (Result<[SchoolDataModel], AppErrors>) -> Void) {
        isFetchDataCalled = true
        if let mockResult = mockResult {
            completion(mockResult)
        }
    }
    
    var fetchDetailMockResult: Result<[SchoolDetailDataModel], AppErrors>?
    var isFetchDetailCalled = false
    func fetchDetail(id: String, completion: @escaping (Result<[SchoolDetailDataModel], AppErrors>) -> Void) {
        isFetchDetailCalled = true
        if let fetchDetailMockResult = fetchDetailMockResult {
            completion(fetchDetailMockResult)
        }
    }
}

extension SchoolDataModel {
    static func mock() -> [SchoolDataModel] {
        [SchoolDataModel(dbn: "123", school_name: "mock", overview_paragraph: "mock", campus_name: "mock")]
    }
}

