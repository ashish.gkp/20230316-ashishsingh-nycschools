//
//  FlowController.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import UIKit

// All possible business flows
enum Destination {
    case nycListScreen
    case detailScreen(SchoolDetailDataModel)
}

protocol FlowControllable {
    func handleNavigation(destination: Destination)
}

//******************************************************************************
// FlowController will control the business flow
// Screen should not know about business flow
//******************************************************************************

class FlowController: FlowControllable {
    private let navController: UINavigationController?
    private let networkManager: NetworkHandler

    init(navController: UINavigationController?,
         networkManager: NetworkHandler) {
        self.navController = navController
        self.networkManager = networkManager
    }

    // handle navigation to show next screen
    func handleNavigation(destination: Destination) {
        switch destination {
        case .nycListScreen:
            let viewModel = SchoolsViewModel(
                flowController: self,
                networkManager: networkManager)
            DispatchQueue.main.async {
                let vc = SchoolsViewController(viewModel: viewModel)
                self.navController?.pushViewController(vc, animated: true)
            }

        case .detailScreen(let dataModel):
            let viewModel = SchoolDetailViewModel(
                flowController: self,
                networkManager: networkManager, dataModel: dataModel)
            DispatchQueue.main.async {
                let vc = SchoolDetailViewController(viewModel: viewModel)
                self.navController?.pushViewController(vc, animated: true)
            }
        }
    }
}
